//////////////////////////
// Das richtige Zeug laden
//////////////////////////

//GET-Parameter aus URL lesen
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

//CSS laden
function loadTheme(thSource) {
    $.ajax({
        url: thSource,
        dataType: "script",
        success: function(result){
            $("head").append("<style>" + result + "</style>");
        },
        statusCode: {
            404: function() {
                alert("CSS not found");
            }
        }
    });
}


//////////////////////////
// Browser/Menu-Führung
//////////////////////////

//Toogle Dropdowns
function toogleLoginForm() {
    if($( "#loginForm" ).is(":hidden")) {
        $( "#loginForm" ).slideDown("slow");
    }
    else {
        $( "#loginForm" ).slideUp("slow");
    }
}

function toogleUserEdit(point){
     point.parent().children(".setpwdForm").slideUp("fast");
     if(point.parent().children(".usermodForm").is(":hidden")) {
        point.parent().children(".usermodForm").slideDown("slow");
    }
    else {
        point.parent().children(".usermodForm").slideUp("slow");
    }
}

function toogleUserPwd(point) {
    point.parent().children(".usermodForm").slideUp("fast");
    if(point.parent().children(".setpwdForm").is(":hidden")) {
        point.parent().children(".setpwdForm").slideDown("slow");
    }
    else {
        point.parent().children(".setpwdForm").slideUp("slow");
    }
}

//Unterordner anzeigen
function toogleSubnav(point) {
    
    if(point.parent().children('ul').is(":hidden")) {
        point.parent().children('ul').slideDown("slow");
    }
    else {
        point.parent().children('ul').slideUp("slow");
    }
}

//Formular anzeigen für Weiterleitung
function toogleForm(point) {
    
    if(point.parent().children('.submitForm').is(":hidden")) {
        point.parent().children('.submitForm').slideDown("slow");
    }
    else {
        point.parent().children('.submitForm').slideUp("slow");
    }
}


///////////////////////////////////////////
// Anzeige-typisch Inhalte rauskommentieren
///////////////////////////////////////////

//Markdown in einzelne Folien aufspliten
function splitSlides(mdSource) {
    singleSlides = mdSource.split("\n---\n");
    return singleSlides;
}

//in jeder Folie Inhalte selektiv rauskommentieren
//2. Parameter:
//  'display': display-Variante, Präsentationsnotizen ('???' als Marker) werden kommentiert
//  'presentation': presentation-Variante, Zusatztexte für Display ('!!!' als Marker) werden kommentiert
function insertComments(allSlides, dispType) {
    for(var i= 0; i < allSlides.length; i++) {
        commentedSlide = "";
        if(dispType == 'display') slideParts = allSlides[i].split("\n!!!\n");
        else if(dispType == 'presentation') slideParts = allSlides[i].split("\n???\n");
        for(var j=0; j < slideParts.length; j++) {
            if(dispType == 'display') hasNote = slideParts[j].indexOf("???\n");
            else if(dispType == 'presentation') hasNote = slideParts[j].indexOf("!!!\n");
            if(hasNote > -1) {
                if(dispType == 'display') slideParts[j] = slideParts[j].replace("???\n", "<!--\n");
                else if(dispType == 'presentation') slideParts[j] = slideParts[j].replace("!!!\n", "<!--\n");
                slideParts[j] = slideParts[j].concat("\n-->\n");
            }
            commentedSlide = commentedSlide.concat(slideParts[j]);
            if(j != slideParts.length -1) {
                if(dispType == 'display') commentedSlide = commentedSlide.concat("\n\n");
                else if(dispType == 'presentation') commentedSlide = commentedSlide.concat("\n???\n");
            }
        }
        allSlides[i] = commentedSlide;
    }
    return allSlides;
}

//aufgesplitete Folien werden wieder zusammengesetzt
function joinSlides(allSlides) {
    joinedSlides  = "";
    for(var i= 0; i < allSlides.length; i++) {
        joinedSlides = joinedSlides.concat(allSlides[i]);
        if(i != allSlides.length -1) {
            joinedSlides = joinedSlides.concat("\n---\n");
        }
    }
    return joinedSlides;
}

//alles in eine nette Funktion packen
//2. Parameter wird an insertComments() weitergegeben: 'display'|'presentation'
function selectiveComments(mdSource, dispType) {
    allSlides = splitSlides(mdSource);
    allSlides = insertComments(allSlides, dispType);
    return joinSlides(allSlides);
}