<div class="attrtext">
    <h2>Cloudmark</h2>
    
    by <a href="https://buero.io" target="_blank">Solutionsbüro</a>
        
    <h3>Cloudmark on GitLab:</h3>
    <a href="https://gitlab.com/solutionsbuero/cloudmark-dev" target="_blank">https://gitlab.com/solutionsbuero/cloudmark-dev</a>

    <h3>This software is heavily based on the following libraries:</h3>
    <p>
        <em>remark</em><br />
        <a href="https://github.com/gnab/remark/" target="_blank">https://github.com/gnab/remark</a><br />
        <a href="https://github.com/gnab/remark/blob/develop/LICENSE" target="_blank">> License</a>
    </p>
    
    <p>
        <em>markdown-it</em><br />
        <a href="https://github.com/markdown-it/markdown-it" target="_blank">https://github.com/markdown-it/markdown-it</a><br />
        <a href="https://github.com/markdown-it/markdown-it/blob/master/LICENSE" target="_blank">> License</a>
    </p>
    
    <p>
        <em>js-base64</em><br />
        <a href="https://github.com/dankogai/js-base64" target="_blank">https://github.com/dankogai/js-base64</a><br />
        <a href="https://github.com/dankogai/js-base64/blob/master/LICENSE.md" target="_blank">> License</a>
    </p>
    
    <p>
        <em>Icons by Smashicons from Flaticon</em><br />
        <a href="https://smashicons.com" target="_blank">https://smashicons.com</a><br />
        <a href="https://file000.flaticon.com/downloads/license/license.pdf" target="_blank">> License</a>
    </p>
</div>