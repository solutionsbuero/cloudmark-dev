<?php
// Choose entry to preselect
if(empty($_GET['presel'])) {$presel = $_SESSION['usrid'];}
else {$presel = $_GET['presel'];}
?>
<ul>
    <li>
        <h4>Choose user to edit:</h4>
        <p><select id="iddrop" onchange="get_usrconfig($(this).val());"></select></p>
    </li>


    <li>
        <h4>Set working directory:</h4>
        <form id="set_rootdir" action="usrapi.php/usr/" method="post">
            <?php
            $browse = new mdBrowser;
            $browse->scanThemes();
            $files = $browse->scandir_rec('mddata/markdown', TRUE);
            echo $browse->dirOutput;
            ?>
            <p><input type="submit" value="submit" /></p>
        </form>
    </li>
    
    <li>
        <h4>Set password</h4>
        <div id="passwd_con"></div>
    </li>
    
    <li>
        <h4>Set privilege level</h4>
        <div id="usrlvlform_con"></div>
    </li>
    
    
    <li>
        <h4>Set presenter configuration</h4>
        <div id="usrconfigform_con"></div>
    </li>
</ul>

<script type="text/javascript">
    
    window.usrid = <?php echo $presel; ?>;
    
    function allusr_callback(usrdata) {
        var tmpl = $.templates("#iddrop_template");
        // write preselection to user list
        for(var i=0; i < usrdata.length; i++) {
            if(usrdata[i].id == window.usrid) {
                usrdata[i].selected = true;
            }
        }
        usrdict = {usr: usrdata};
        iddrop = tmpl.render(usrdict);
        $("#iddrop").html(iddrop);
        get_usrconfig($("#iddrop").val());
    }
    
    $(function() {
        load_all_users();
    });
    
    $("#set_rootdir").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var url = form.attr('action') + window.usrid;
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data) {
                $("#feedback").html("<p class='notice'>" + data + "</p>");
                get_usrconfig(window.usrid);
            },
            error: function(data) {
                $("#feedback").html("<p class='error'>" + data.responseText + "</p>");
            }
        });
    });
</script>