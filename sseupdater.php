<?php
session_start();
include("includes/lib/sseManager.php");
$sse_manager = new SSEManager();
$sse_manager->connect();
if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['slide'])) {
    $sse_manager->sse_update($_POST['slide']);
    header("HTTP/2.0 200");
    echo "";
}
else {
    header("HTTP/2.0 400");
    echo "";
}
$sse_manager->disconnect();
?>