<?php

$debug = TRUE;
$feedback = "";

if($debug) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL & ~E_NOTICE);
}

include("includes/lib/mdBrowser.php");
include("includes/lib/sessionManager.php");
//include("includes/lib/usr_db_mod.php");

session_start();

// Session Management
$session_manager = new SessionManager();
$session_manager->connect();
if($_GET['session'] == "login") {
    $feedback .= $session_manager->login();
}

elseif($_GET['session'] == "logout") {
    $feedback .= $session_manager->logout();
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Cloudmark</title>
    <link rel="icon" href="img/icon.png" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/jquery-template.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/1.0.2/jsrender.min.js"></script>
    <script src="js/base64.js"></script>
	<script src="js/utilities.js"></script>
    <script src="js/sharedapi.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Italianno" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet"> 
	<link href="style/browse.css" rel="stylesheet" type="text/css" />
    <!--<link href="style/toogleswitch.css" rel="stylesheet" type="text/css" />-->
    <!-- START jsrender templates -->
    <?php include("includes/templates/jsrender.php"); ?>
    <!-- END jsrender templates -->
    
</head>

<body>
    
    <?php include("includes/templates/header.php"); 
        
        if($_SESSION['usrlvl'] == 2 && $_GET['type'] == 'usrlist') {
            include("includes/templates/usrlist.php");
        }
        
        if($_GET['type'] == 'usrconf' && $_SESSION['usrlvl'] == 1) {
            include("includes/templates/usrconfig.php");
        }
        
        if($_GET['type'] == 'usrconf' && $_SESSION['usrlvl'] == 2) {
            include("includes/templates/adminconfig.php");
        }
                
        if($_GET['type'] == "attr") {
            include("includes/templates/attribution.php");
        }
            
        if(empty($_GET['type'])) {
            include("includes/templates/browser.php");
        }
		?>
</body>
<?php $session_manager->disconnect(); ?>
</html>