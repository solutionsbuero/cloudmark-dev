<?php
include("includes/lib/usrManager.php");
$path = array_values(array_diff(explode('/',$_SERVER['REQUEST_URI']), array("", "usrapi.php")));
session_start();

$usr_manager = new UsrManager();
$usr_manager->connect();

// Create new user
if($path[0] == "usr" && empty($path[1]) && $_SERVER['REQUEST_METHOD'] == "POST") {
    if($_SESSION['usrlvl'] != 2) {
        header("HTTP/2.0 401");
        echo "Permission denied";
    }
    elseif(empty($_POST['usrname'])) {
        header("HTTP/2.0 400");
        echo "Bad request";
    }
    else {
        $reval = $usr_manager->create_usr($_POST['usrname']);
        header("HTTP/2.0 ".$reval['status']);
        echo $reval['feedback'];
    }
}

// Get/modifiy a user
elseif($path[0] == "usr" && !empty($path[1])) {
    $id = $path[1];
    
    // Check permission
    if($_SESSION['usrlvl'] != 2 && ($_SESSION['usrid'] != $id || $_SESSION['usrlvl'] == 0)) {
        header("HTTP/2.0 401");
        echo "Permission denied";
    }
    
    // Check method -> What to do?
    
    // Get configuration
    elseif($_SERVER['REQUEST_METHOD'] == 'GET') {
        $reval = $usr_manager->get_config($id);
        header("HTTP/2.0 ".$reval['status']);
        echo $reval['feedback'];
    }
    
    // Change password
    elseif($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['passwd'])) {
        if($_POST['passwd'] != $_POST['passwd2']) {
            header("HTTP/2.0 406");
            echo "Password missmatch";
        }
        else {
            $reval = $usr_manager->change_passwd($id, $_POST['passwd']);
            header("HTTP/2.0 ".$reval['status']);
            echo $reval['feedback'];
        }
    }
    
    //Change user working directory
    elseif($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['rootdir']) && $_SESSION['usrlvl'] == 2) {
        $reval = $usr_manager->change_rootdir($id, $_POST['rootdir']);
        header("HTTP/2.0 ".$reval['status']);
        echo $reval['feedback'];
    }
    
    //Change user privilege level
    elseif($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['usrlvl']) && $_SESSION['usrlvl'] == 2) {
        $reval = $usr_manager->change_usrlvl($id, $_POST['usrlvl']);
        header("HTTP/2.0 ".$reval['status']);
        echo $reval['feedback'];
    }
    
    //Change presenter configuration
    elseif($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['aspect'])) {
        $reval = $usr_manager->change_config($id, $_POST);
        header("HTTP/2.0 ".$reval['status']);
        echo $reval['feedback'];
    }
    
    // Delete user
    elseif($_SERVER['REQUEST_METHOD'] == 'DELETE') {
        $reval = $usr_manager->delete_usr($id);
        header("HTTP/2.0 ".$reval['status']);
        echo $reval['feedback'];
    }
    
    else {
        header("HTTP/2.0 401");
        echo "Permission denied";
    }
    
}

// Get all users
if($path[0] == "usr"&& empty($path[1]) && $_SERVER['REQUEST_METHOD'] == "GET") {
    
    // Check permissions
    if($_SESSION['usrlvl'] != 2) {
        header("HTTP/2.0 401");
        echo "Permission denied";
    }
    
    // Check request
    elseif($_SERVER['REQUEST_METHOD'] != 'GET') {
        header("HTTP/2.0 400");
        echo "Bad request";
    }
    
    // Conditions satisfied, get user list
    else {
        $reval = $usr_manager->get_all_usrs();
        header("HTTP/2.0 ".$reval['status']);
        echo $reval['feedback'];
    }
}

$usr_manager->disconnect();
?>