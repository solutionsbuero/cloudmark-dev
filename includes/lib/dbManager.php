<?php

include("includes/lib/dbCredentials.php");

class DBManager extends dbCred {
    
    protected $con = null;
    
    function connect() {
        $this->con = new mysqli($this->dbhost, $this->dbusr, $this->dbpasswd, $this->db);
        return $this->con;
    }
    
    function disconnect() {
        $this->con->close();
    }
    
}
?>