<?php
include("includes/lib/rmHelper.php");
$rm_helper = new RMHelper();
$rm_helper->connect();
$JSONconfig = $rm_helper->json_config();
$rm_helper->disconnect();
?>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Cloudmark Presentation</title>
    <link rel="icon" href="img/icon.png" />
    <style></style>
    <link href="style/presentation.css" rel="stylesheet" type="text/css" />
    <script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/base64.js"></script>
    <script src="js/utilities.js"></script>
    <script src="js/sse.js"></script>
    
    <script type="text/javascript">
        
        function presMD(mdSource) {
            $.ajax({
                url: decodeURIComponent(mdSource),
                success: function(result){
                    $("#source").text(selectiveComments(result, 'presentation'));
                    window.slideshow = remark.create(JSON.parse('<?php echo $JSONconfig; ?>'));
                    
                    <?php if($_GET['sse'] == "push") { ?>
                    window.slideshow.on('showSlide', function(slide) {
                        update_sse(slide.getSlideIndex());
                    });
                    
                    <?php } if($_GET['sse'] == "pull") { ?>
                    var source = new EventSource("ssepublisher.php", { withCredentials: true });
                    source.onopen = function() {
                        console.log("Connection to server opened.");
                    };
                    source.onmessage = function(event) {
                        console.log("received " + event.data);
                        if(event.data != "timeout") {
                            window.slideshow.gotoSlide(parseInt(event.data) + 1);
                        }
                    };
                    <?php } ?>
                    
                },
                statusCode: {
                    404: function() {
                        $("#error").text("markdown file not found");
                        $("#source").hide();
                        $("#error").show();
                    }
                }
            });
        }
        
        loadTheme(Base64.decode(GetURLParameter("theme")));
        presMD(Base64.decode(GetURLParameter("src")));
    </script>
    
</head>

<body>
    <?php if($_GET['touch'] == 1) { ?>
    <div id="touchbar">
        <a href="javascript:window.slideshow.gotoPreviousSlide();">&larr;</a>
        <a href="javascript:window.slideshow.gotoNextSlide();">&rarr;</a>
        <a href="javascript:window.slideshow.togglePresenterMode();">P</a>
        <a href="javascript:window.slideshow.toggleFullscreen();">F</a>
    </div>
    <?php } ?>
    <textarea id="source"></textarea>
    <div id="error" style="display: none;"></div>
</body>

</html>