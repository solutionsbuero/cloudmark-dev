<?php

include("includes/lib/usrManager.php");

class RMHelper extends UsrManager {
    
    public $usrlvl;
    public $touch;
    public $usrid;
    public $default_config = array(
        'ratio' => '4:3',
        'navigation' => array(
            'scroll' => TRUE,
            'touch' => TRUE,
            'click' => TRUE
        )
    );  
    
    function __construct() {
        $this->usrlvl = $_SESSION['usrlvl'];
        $this->usrid = $_SESSION['usrid'];
        $this->touch = $_GET['touch'];
    }
    
    function json_config() {
        if($this->usrlvl > 0) {
            $r1 = $this->get_config($this->usrid);
            $result = json_decode($r1['feedback'], true);
            if($this->touch == 1) {
                $config = array(
                    'ratio' => $result['aspect'],
                    'navigation' => array(
                        'scroll' => (bool)$result['touch_nav_scroll'],
                        'touch' => (bool)$result['touch_nav_touch'],
                        'click' => (bool)$result['touch_nav_click']
                    )
                );
            }
            else {
                $config = array(
                    'ratio' => $result['aspect'],
                    'navigation' => array(
                        'scroll' => (bool)$result['nav_scroll'],
                        'touch' => (bool)$result['nav_touch'],
                        'click' => (bool)$result['nav_click']
                    )
                );
            }
        }
        else {
            $config = $this->default_config;
        }
        return json_encode($config);
    }
    
}

?>