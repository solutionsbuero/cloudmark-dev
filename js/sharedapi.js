function load_all_users() {
    $.ajax({
        url: "usrapi.php/usr",
        type: "GET",
        success: function(data) {
            usrdata = JSON.parse(data);
            allusr_callback(usrdata);
        },
        error: function(data) {
            $("#feedback").html("<p class='error'>" + data.responseText + "</p>");
        }
    });
}

function get_usrconfig(usrid) {
    $.ajax({
        url: "usrapi.php/usr/" + usrid,
        type: "GET",
        success: function(data) {
            window.usrid = usrid;
            usrdata = JSON.parse(data);
            if($("#usrlvlform_con").length) {
                render_usrlvl(usrdata);
            }
            render_passwd(usrdata);
            render_usrconfig(usrdata);
            
        },
        error: function(data) {
            $("#feedback").html("<p class='error'>" + data.responseText + "</p>");
        }
    });
}

function render_passwd(usrdata) {
    var tmpl = $.templates("#passwdform_template");
    usrconfig = tmpl.render(usrdata);
    $("#passwd_con").html(usrconfig);
}

function render_usrlvl(usrdata) {
    var tmpl = $.templates("#usrlvlform_template");
    usrconfig = tmpl.render(usrdata);
    $("#usrlvlform_con").html(usrconfig);
}

function render_usrconfig(usrdata) {
    var tmpl = $.templates("#usrconfigform_template");
    usrconfig = tmpl.render(usrdata);
    $("#usrconfigform_con").html(usrconfig);
}