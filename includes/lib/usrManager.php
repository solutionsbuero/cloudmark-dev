<?php

include("includes/lib/dbManager.php");

class UsrManager extends DBManager {
    
    function create_usr($usrname) {
        
        $return_value = array();
        $unique = $this->con->query("SELECT usrname FROM usr WHERE usrname='$usrname'");
        
        // Check Unique condition
        if($unique->num_rows > 0 || empty($usrname)) {
            $return_value['status'] = 406;
            $return_value['feedback'] = "Username already exists or is empty";
            return $return_value;
        }
        
        // All conditions fullfilled, create user 
        else {
            $this->con->query("INSERT INTO usr (usrname) VALUES ('$usrname')");
            $result = $this->con->query("SELECT id FROM usr WHERE usrname = '$usrname'");
            $row = $result->fetch_assoc();
            $usrid = $row['id'];
            $this->con->query("INSERT INTO sse_events (usrid) VALUES ('$usrid')");
            $return_value['status'] = 202;
            $return_value['feedback'] = $usrid;
            return $return_value;
        }
    }
    
    function change_passwd($id, $new_passwd) {
        $pwdhash = hash('sha256', $new_passwd);
        $this->con->query("UPDATE usr SET pwdhash='$pwdhash' WHERE id=$id");
        $return_value['status'] = 202;
        $return_value['feedback'] = "Password updated";
        return $return_value;
    }
    
    function change_rootdir($id, $rootdir) {
        $this->con->query("UPDATE usr SET rootdir='$rootdir' WHERE id=$id");
        $return_value['status'] = 202;
        $return_value['feedback'] = "Working directory updated";
        return $return_value;
    }
    
    function change_usrlvl($id, $usrlvl) {
        $this->con->query("UPDATE usr SET usrlvl=$usrlvl WHERE id=$id");
        $return_value['status'] = 202;
        $return_value['feedback'] = "Privilege level updated";
        return $return_value;
    }
    
    function change_config($id, $config_array) {
        $sql = "UPDATE usr SET ";
        $keys = array_keys($config_array);
        $n = 0;
        $m = sizeof($config_array) -1;
        foreach($keys as $key) {
            $value = $config_array[$key];
            if(is_array($value)) {$value = max($value);}
            $sql .= "$key='$value'";
            if($n < $m) {$sql .= ", ";}
            $n++;
        }
        $sql .= " WHERE id=$id";
        $this->con->query($sql);
        $return_value['status'] = 202;
        $return_value['feedback'] = "Config updated";
        //$return_value['feedback'] = $sql;
        return $return_value;
    }
    
    function delete_usr($id) {
        $this->con->query("DELETE FROM usr WHERE id=$id");
        $this->con->query("DELETE FROM sse_events WHERE usrid=$id");
        $return_value['status'] = 202;
        $return_value['feedback'] = "User removed";
        return $return_value;
    }
    
    function get_config($id) {
        $result = $this->con->query("SELECT * FROM usr WHERE id=$id");
        $row = $result->fetch_assoc();
        unset($row['pwdhash']);
        $return_value['status'] = 200;
        $return_value['feedback'] = json_encode($row);
        return $return_value;
    }
    
    function get_all_usrs () {
        $usrlist = [];
        $result = $this->con->query("SELECT id, usrname FROM usr ORDER BY usrname");
        while($row = $result->fetch_assoc()) {
            $usrlist[] = $row;
        }
        $return_value['status'] = 200;
        $return_value['feedback'] = json_encode($usrlist);
        return $return_value;
    }
    
}
?>