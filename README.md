# Cloudmark

by [Solutionsbüro](https://buero.io)

A php/js based system to present and prettily display markdown files with the long term goal to free us all from dreadful Microsoft PowerPoint.

_Cloudmark_ wraps [remark.js](https://github.com/gnab/remark) and [markdown-it](https://github.com/markdown-it/markdown-it) with a file browser, a responsive interface and a SSE based api to sync presentation between multiple devices.

_Cloudmark_ relies only on techonologies, that are usually available on any shared web host and can therefor be deployed easily in many environments.

This software is a work in progress. Although a lot is already in working order, there are still a lot of planed and unfinished features. Some of those include:
- Export to pdf, maybe?
- better aproach to upload and asset managment, possibly sabre/dav
- WYSIWYG editor for markdown files, possibly stackedit.js

Any form of contribution and/or collaboration is very welcome.

### Database config:

Cloudmark depends on a MySQL database.

- Set database credentials in `includes/lib/dbCredentials.php`.
- Write `install.sql` to database

This process installs a default user with administration rights, using the follwing credentials:

User: `admin`

Password: `admin`

### This software is heavily based on the following libraries:

[remark](https://github.com/gnab/remark)

[markdown-it](https://github.com/markdown-it/markdown-it)

[js-base64](https://github.com/dankogai/js-base64)

[Icons by Smashicons from Flaticon](https://smashicons.com)
