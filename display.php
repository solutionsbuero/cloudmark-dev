<!DOCTYPE html>
<html>
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Cloudmark Display</title>
    <link rel="icon" href="img/icon.png" />
    <!--<script src="js/showdown.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/markdown-it/8.4.2/markdown-it.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/base64.js"></script>
    <script src="js/utilities.js"></script>
    <style></style>
    
    <script type="text/javascript">
        
        function dispMD(mdSource) {
            $.ajax({
                url: mdSource,
                success: function(result) {
                    var md = window.markdownit({
                        html: true
                    });
                    result = selectiveComments(result, 'display');
                    var rendered = md.render(result);
                    $("#md").html(rendered);
                },
                statusCode: {
                    404: function() {
                        $("#md").html("markdown file not found");
                    }
                }
            });
        }
        
        loadTheme(Base64.decode(GetURLParameter("theme")));
        dispMD(Base64.decode(GetURLParameter("src")));        
        
    </script>
</head>

<body>
    <div id="md"></div>
</body>

</html>