<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

session_start();

include("includes/lib/sseManager.php");
$sse_manager = new SSEManager();
$sid = session_id();
$sse_manager->connect();
$init_result = $sse_manager->sse_check();
$timeout = time() + 50;
while(1) {
    $result = $sse_manager->sse_check();
    if($result['session_id'] != $sid && $result['slide'] != $init_result['slide']) {
        echo "data:".$result['slide'].PHP_EOL;
        break;
    }
    elseif(time() > $timeout) {
        echo "data:timeout".PHP_EOL;
        break;
    }
}
echo "retry:100".PHP_EOL;
echo PHP_EOL;
ob_end_flush();
flush();
$sse_manager->disconnect();
?>