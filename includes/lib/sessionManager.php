<?php

include("includes/lib/dbManager.php");

class SessionManager extends DBManager {
    
    function login() {
        $usr = $_POST['usr'];
        $pwdhash = hash('sha256', $_POST['pwd']);
        $result = $this->con->query("SELECT id, usrname, pwdhash, usrlvl, rootdir FROM usr WHERE usrname='$usr'");
        $row = $result->fetch_assoc();
        if($row != FALSE && $row['pwdhash'] == $pwdhash) {
            $_SESSION['logged'] = TRUE;
            $_SESSION['usrid'] = (int)$row['id'];
            $_SESSION['usrlvl'] = (int)$row['usrlvl'];
            $_SESSION['rootdir'] = $row['rootdir'];
            return "<p class='notice'>Logged-in as ".$row['usrname']."</p>";
        }
        else {
            return "<p class='warning'>Sorry, access denied.</p>";
        }
    }
    
    function logout() {
        unset($_SESSION);
        session_destroy();
        return "<p class='notice'>You logged out</p>";
    }
}

?>