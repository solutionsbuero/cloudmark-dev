<!-- User List template -->
<script id="usrlist_template" type="text/x-jsrender">
    <li class="usrentry">
        <a class="editicons" href="javascript: usrdel({{:id}});"><img src="img/008-delete.svg" /></a>
        <a class="editicons" href="index.php?type=usrconf&presel={{:id}}"><img src="img/010-settings.svg" /></a>
        {{:usrname}}
    </li>
</script>

<!-- ID Dropdown template -->
<script id="iddrop_template" type="text/x-jsrender">
    {{for usr}}
    <option value="{{:id}}" {{if selected}} selected="selected" {{/if}}>{{:usrname}}</option>
    {{/for}}
</script>

<!-- User level form template -->
<script id="usrlvlform_template" type="text/x-jsrender">
    <form id="set_usrlvl" action="usrapi.php/usr/" method="post">
        Privilege level: <select name="usrlvl" id="usrlvl">
            <option value="0" {{if usrlvl==0}}selected='selected'{{/if}}>0 - readonly</option>
            <option value="1" {{if usrlvl==1}}selected='selected'{{/if}}>1 - contributer</option>
            <option value="2" {{if usrlvl==2}}selected='selected'{{/if}}>2 - admin</option>
        </select>
        <p><input type="submit" value="submit" /></p>
    </form>
    <script>
    $("#set_usrlvl").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var url = form.attr('action') + window.usrid;
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data) {
                $("#feedback").html("<p class='notice'>" + data + "</p>");
                get_usrconfig(window.usrid);
            },
            error: function(data) {
                $("#feedback").html("<p class='error'>" + data.responseText + "</p>");
            }
        });
    });
    </script>
</script>

<script id="passwdform_template" type="text/x-jsrender">
    <form id="set_passwd" action="usrapi.php/usr/" method="post">
        <p>Password: </p><input type="password" name="passwd" />
        <p>Repeat: </p><input type="password" name="passwd2" />
        <p><input type="submit" value="submit" /></p>
    </form>
    <script>
    $("#set_passwd").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var url = form.attr('action') + window.usrid;
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data) {
                $("#feedback").html("<p class='notice'>" + data + "</p>");
                get_usrconfig(window.usrid);
            },
            error: function(data) {
                $("#feedback").html("<p class='error'>" + data.responseText + "</p>");
            }
        });
    });
    </script>
</script>

<!-- Persenter options config form template -->
<script id="usrconfigform_template" type="text/x-jsrender">
    <form id="set_config" action="usrapi.php/usr/" method="post">
        Aspect ratio: <select name="aspect" id="aspect">
            <option value="4:3" {{if aspect=='4:3'}}selected='selected'{{/if}}>4:3</option>
            <option value="16:9" {{if aspect=='16:9'}}selected='selected'{{/if}}>16:9</option>
        </select>
        
        <p>
            Events to navigate through slides, <strong>presenter mode:</strong><br />
            <input type="hidden" name="nav_scroll" value="0" />
            <input type="checkbox" name="nav_scroll" value="1" {{if nav_scroll==1}} checked="checked" {{/if}}/>
            Scroll
            <br />
            <input type="hidden" name="nav_touch" value="0" />
            <input type="checkbox" name="nav_touch" value="1" {{if nav_touch==1}} checked="checked" {{/if}}/>
            Touch
            <br />
            <input type="hidden" name="nav_click" value="0" />
            <input type="checkbox" name="nav_click" value="1" {{if nav_click==1}} checked="checked" {{/if}}/>
            Click
        </p>
        <p>
            Events to navigate through slides, <strong>touch-presenter mode:</strong><br />
            <input type="hidden" name="touch_nav_scroll" value="0" />
            <input type="checkbox" name="touch_nav_scroll" value="1" {{if touch_nav_scroll==1}} checked="checked" {{/if}}/>
            Scroll
            <br />
            <input type="hidden" name="touch_nav_touch" value="0" />
            <input type="checkbox" name="touch_nav_touch" value="1" {{if touch_nav_touch==1}} checked="checked" {{/if}}/>
            Touch
            <br />
            <input type="hidden" name="touch_nav_click" value="0" />
            <input type="checkbox" name="touch_nav_click" value="1" {{if touch_nav_click==1}} checked="checked" {{/if}}/>
            Click
        </p>
        <p><input type="submit" value="submit" /></p>
    </form>
    <script>
    $("#set_config").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var url = form.attr('action') + window.usrid;
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data) {
                $("#feedback").html("<p class='notice'>" + data + "</p>");
                get_usrconfig(window.usrid);
            },
            error: function(data) {
                $("#feedback").html("<p class='error'>" + data.responseText + "</p>");
            }
        });
    });
    </script>
</script>