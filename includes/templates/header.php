<div id="headerWrapper">
    <div id="header">
        
        <h1><a href="index.php">C<span class="desktop">loud</span>m<span class="desktop">ark</span></a></h1>
        
        <?php if(!$_SESSION['logged']) { ?>
            <a href="#" onclick="toogleLoginForm();">
                <span class="desktop">Login</span>
                <span class="mobile"><img src="img/006-lock-closed.svg" /></span>
            </a>
            
        <?php } else { ?>
            <a href="index.php?session=logout">
                <span class="desktop">Logout</span>
                <span class="mobile"><img src="img/005-lock-open.svg" /></span>
            </a>
        <?php }
        
        if($_SESSION['usrlvl'] == 2) { ?>
            <a href="index.php?type=usrlist">
                <span class="desktop">Users</span>
                <span class="mobile"><img src="img/002-users.svg" /></span>
            </a>
        <?php }
        
        if($_SESSION['usrlvl'] >= 1) { ?>
            <a href="index.php?type=usrconf">
                <span class="desktop">Settings</span>
                <span class="mobile"><img src="img/010-settings.svg" /></span>
            </a>
        <?php } ?>
        
        <a href="index.php?type=attr">
            <span class="desktop">Attribution</span>
            <span class="mobile"><img src="img/004-share.svg" /></span>
        </a>
        
        <div id="feedback"><?php echo $feedback; ?></div>     
        
        <div id="loginForm">
            <form action="index.php?session=login" method="post">
            Username: <br class="mobile" />
            <input type="text" name="usr" /><br class="mobile" />
            Password:<br class="mobile" />
            <input type="password" name="pwd" /><br class="mobile" />
            <input type="submit" value="submit" />
            </form>
        </div>
    </div>
</div>