<ul id="usrlist">
    <li>
        <form id="usrcreate" action="usrapi.php/usr" method="post">
            <em>Create new user: </em><br class="mobile" />
            Username: <br class="mobile" />
            <input type="text" name="usrname" /><br class="mobile" />
            <input type="submit" value="submit" />
        </form>
    </li>
</ul>

<script type="text/javascript">
    
    function usrdel(id) {
        conf_dialog = confirm("Sure about that?");
        if(conf_dialog) {
            $.ajax({
                type: "DELETE",
                url: "usrapi.php/usr/" + id,
                success: function() {
                    $("#feedback").html("<p class='notice'>User deleted</p>");
                    load_all_users();
                }
            });
        }
    }
    
    function allusr_callback(usrdata) {
        var tmpl = $.templates("#usrlist_template");
        console.log(usrdata);
        if(usrdata != null) {
            usrlist = "";
            for(var i = 0; i < usrdata.length; i++) {
                usrlist += tmpl.render(usrdata[i]);
            }
            $( ".usrentry" ).remove();
            $("#usrlist").append(usrlist);
        }
    }
    
    // Attach events - user creation
    $("#usrcreate").submit(function(e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.
        var form = $(this);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data) {
                $("#feedback").html("<p class='notice'>User created, <a href='index.php?type=usrconf&presel=" + data + "'>edit user config</a></p>");
                load_all_users();
            },
            error: function(data) {
                $("#feedback").html("<p class='error'>" + data.responseText + "</p>");
            }
        });
    });
    
    // Initialize user data
    $(function() {
        load_all_users();
    });
    
</script>