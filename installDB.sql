-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 21, 2019 at 07:21 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cloudmark`
--

-- --------------------------------------------------------

--
-- Table structure for table `sse_events`
--

CREATE TABLE `sse_events` (
  `id` int(11) NOT NULL,
  `usrid` int(11) NOT NULL,
  `session_id` varchar(50) NOT NULL,
  `slide` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sse_events`
--

INSERT INTO `sse_events` (`id`, `usrid`, `session_id`, `slide`, `time`) VALUES
(1, 1, '', 0, '2019-04-21 17:20:28');

-- --------------------------------------------------------

--
-- Table structure for table `usr`
--

CREATE TABLE `usr` (
  `id` int(11) NOT NULL,
  `usrname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pwdhash` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `usrlvl` int(11) NOT NULL,
  `rootdir` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `aspect` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '16:9',
  `nav_scroll` tinyint(1) NOT NULL DEFAULT '1',
  `nav_touch` tinyint(1) NOT NULL DEFAULT '1',
  `nav_click` tinyint(1) NOT NULL DEFAULT '1',
  `touch_nav_scroll` tinyint(1) NOT NULL DEFAULT '0',
  `touch_nav_touch` tinyint(1) NOT NULL DEFAULT '0',
  `touch_nav_click` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usr`
--

INSERT INTO `usr` (`id`, `usrname`, `pwdhash`, `usrlvl`, `rootdir`, `aspect`, `nav_scroll`, `nav_touch`, `nav_click`, `touch_nav_scroll`, `touch_nav_touch`, `touch_nav_click`) VALUES
(1, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 2, 'mddata/markdown/_protected', '4:3', 1, 1, 1, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sse_events`
--
ALTER TABLE `sse_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usr`
--
ALTER TABLE `usr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usrname` (`usrname`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sse_events`
--
ALTER TABLE `sse_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usr`
--
ALTER TABLE `usr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;