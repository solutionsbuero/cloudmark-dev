<?php

function base64url_encode($data) { 
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 
function base64url_decode($data) { 
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
} 

if($_POST['mode'] == "present" || $_POST['mode'] == "touchpresent") {
    $url = "presentation.php?sse=".$_POST['sse']."&";
}
else {
    $url = "display.php?";
}

if($_POST['mode'] == "touchpresent") {
    $url .= "touch=1&";
}

if($_POST['sse'])

$url .= "src=";
$url .= base64url_encode($_POST['path']);
$url .= "&theme=";
$url .= base64url_encode($_POST['themeFolder'] . "/" . $_POST['theme']);

header('Location: '.$url);
die();