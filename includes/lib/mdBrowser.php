<?php

class mdBrowser {
    var $html;
    var $dirOutput;
    var $themeFolder = "mddata/themes";
	var $themeOptions;
	
    function scandir_rec($root, $setroot=FALSE) {
        
        $dirs = scandir($root);
        asort($dirs);
        $fileOutput = "";
        foreach ($dirs as $dir) {
            
            if ($dir == '.' || $dir == '..') {
                continue;
            }
			
            $path = $root . '/' . $dir;
            if (is_file($path)) {
				if($setroot) $fileOutput .= $this->markupFileSetroot($path, $dir);
                else $fileOutput .= $this->markupFile($path, $dir);
            }
            
            else if (is_dir($path)) {
                $this->dirOutput .= '<li class="folder">';
				if($setroot) $this->dirOutput .= $this->markupFolderSetroot($path,$dir);
                else $this->dirOutput .= $this->markupFolder($path,$dir);
                $this->dirOutput .= '<ul>';
                $this->scandir_rec($path, $setroot); // <--- then recursion
                $this->dirOutput .= '</ul>';
                $this->dirOutput .= '</li>';
            }
        
        }
        $this->dirOutput .= $fileOutput;
    }
	
	
	function markupFolder($path, $dir) {
		ob_start();
		?>
		<a href="#" onclick="toogleSubnav($(this));"><img src="img/007-folder.svg" /><?php echo $dir; ?></a>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}
	
	
	function markupFile($path, $dir) {
		ob_start();
		?>
		<li class="file">
			<a class="desktop" ><img src="img/md.svg" /><?php echo $dir; ?></a>
            <div class="desktop inlineForm">
                <form action="redirect.php" method="post" target="_blank">
                    <input type="hidden" name="themeFolder" value="<?php echo $this->themeFolder; ?>"/>
					<input type="hidden" name="path" value="<?php echo $path; ?>"/>
                    SSE:
                    <select name="sse">
                        <option value="none">None</option>
                        <option value="push">push</option>
                        <option value="pull">pull</option>
                    </select>
                    Mode:
                    <select name="mode">
                        <option value="display">display</option>
                        <option value="present">present</option>
                        <option value="touchpresent">touch-present</option>
                    </select>
                    Style: <select name="theme"><?php echo $this->themeOptions;?></select>
                    <input type="submit" value="GO!"/>
                </form>
            </div>
            
            <a href="#" class=" mobile" onclick="toogleForm($(this));"><img src="img/md.svg" /><?php echo $dir; ?></a>
			<div class="submitForm mobile">
				<form action="redirect.php" method="post">
					<input type="hidden" name="themeFolder" value="<?php echo $this->themeFolder; ?>"/>
					<input type="hidden" name="path" value="<?php echo $path; ?>"/>
                    Mode:<br />
                    <select name="mode">
                        <option value="display">display</option>
                        <option value="present">present</option>
                        <option value="touchpresent">touch-present</option>
                    </select><br />
                    Style:<br />
                    <select name="theme"><?php echo $this->themeOptions;?></select><br />
                    SSE:<br />
                    <select name="sse">
                        <option value="none">None</option>
                        <option value="push">push</option>
                        <option value="pull">pull</option>
                    </select><br />
                    <input type="submit" value="GO!"/>
				</form>
			</div>
		</li>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}
    
    
    function markupFolderSetroot($path, $dir) {
		ob_start();
		?>
		<input class="setrootInput" type="radio" name="rootdir" value="<?php echo $path; ?>" />
        <a href="#" onclick="toogleSubnav($(this));"><img src="img/007-folder.svg" /><?php echo $dir; ?></a>
		<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}
    
    function markupFileSetroot($path, $dir) {
		/*ob_start();
		?>
		<li class="file">
			<input class="setrootInput" type="radio" name="rootdir" value="<?php echo $path; ?>" />
            <a><img src="img/md.svg" /><?php echo $dir; ?></a>
		</li>
		<?php
		$output = ob_get_contents();
		ob_end_clean();*/
        $output = "";
		return $output;
	}
	
	
	function scanThemes() {
		$nope = array('.','..');
		$themes = scandir($this->themeFolder);
		$themes = array_diff($themes, $nope);
		asort($themes);
		$output = "";
		foreach ($themes as $value) {
			$output.= "<option value='$value'>$value</option>";
		}
		$this->themeOptions = $output;
	}
}

?>